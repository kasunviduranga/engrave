<?php include 'db/dbConnection.php'; ?>
<header>
    <div class="container">
        <!-- top nav -->
        <nav class="top_nav d-flex pt-3 pb-1">
            <!-- logo -->
            <h1>
                <a class="navbar-brand" href="index">ENGRAVE.lk
                </a>
            </h1>
            <!-- //logo -->
            <div class="w3ls_right_nav ml-auto d-flex">
                <!-- search form -->
                <!-- <form class="nav-search form-inline my-0 form-control" action="#" method="post">
                    <select class="form-control input-lg" name="category">
                        <option value="all">Search our store</option>
                        <optgroup label="Mens">
                            <option value="T-Shirts">T-Shirts</option>
                            <option value="coats-jackets">Coats & Jackets</option>
                            <option value="Shirts">Shirts</option>
                            <option value="Suits & Blazers">Suits & Blazers</option>
                            <option value="Jackets">Jackets</option>
                            <option value="Sweat Shirts">Trousers</option>
                        </optgroup>
                        <optgroup label="Womens">
                            <option value="Dresses">Dresses</option>
                            <option value="T-shirts">T-shirts</option>
                            <option value="skirts">Skirts</option>
                            <option value="jeans">Jeans</option>
                            <option value="Tunics">Tunics</option>
                        </optgroup>
                        <optgroup label="Girls">
                            <option value="Dresses">Dresses</option>
                            <option value="T-shirts">T-shirts</option>
                            <option value="skirts">Skirts</option>
                            <option value="jeans">Jeans</option>
                            <option value="Tops">Tops</option>
                        </optgroup>
                        <optgroup label="Boys">
                            <option value="T-Shirts">T-Shirts</option>
                            <option value="coats-jackets">Coats & Jackets</option>
                            <option value="Shirts">Shirts</option>
                            <option value="Suits & Blazers">Suits & Blazers</option>
                            <option value="Jackets">Jackets</option>
                            <option value="Sweat Shirts">Sweat Shirts</option>
                        </optgroup>
                    </select>
                    <input class="btn btn-outline-secondary  ml-3 my-sm-0" type="submit" value="Search">
                </form> -->
                <!-- search form -->
                <div class="nav-icon d-flex">
                    <!-- sigin and sign up -->
                    <a class="text-dark login_btn align-self-center mx-3" href="#myModal_btn" data-toggle="modal"
                        data-target="#myModal_btn">
                        <i class="far fa-user"></i>
                    </a>
                    <!-- sigin and sign up -->
                    <!-- shopping cart -->
                    <div class="cart-mainf">
                        <div class="hubcart hubcart2 cart cart box_1">
                            <form action="#" method="post">
                                <input type="hidden" name="cmd" value="_cart">
                                <input type="hidden" name="display" value="1">
                                <button class="btn top_hub_cart mt-1" type="submit" name="submit" value="" title="Cart">
                                    <i class="fas fa-shopping-bag"></i>
                                </button>
                            </form>
                        </div>
                    </div>
                    <!-- //shopping cart ends here -->
                </div>
            </div>
        </nav>
        <!-- //top nav -->
        <!-- bottom nav -->
        <nav class="navbar navbar-expand-lg navbar-light justify-content-center">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mx-auto text-center">
                    <li class="nav-item">
                        <a class="nav-link  active" href="index">Home
                            <span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <?php
                    $sql ="SELECT * FROM `category_tbl`";
                    $result = mysqli_query($connection,$sql);
                    while($dataRow=mysqli_fetch_assoc($result)){
                        $result2 = $mysqli->query("SELECT * FROM sub_category_tbl WHERE cat_id=$dataRow[category_id]") or die($mysqli->error());
                        if ( $result2->num_rows > 0 ) { ?>
                            <li class="nav-item dropdown has-mega-menu" style="position:static;">
                                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="craft?id=<?php echo $dataRow['category_id'] ?>" role="button"
                                    aria-haspopup="true" aria-expanded="false"><?php echo $dataRow['category_name'] ?></a>
                                <div class="dropdown-menu" style="width:25%; left:auto !important;">
                                    <div class="px-0 container">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php
                                                $sql1 ="SELECT * FROM sub_category_tbl WHERE cat_id=$dataRow[category_id]";
                                                $result1 = mysqli_query($connection,$sql1);
                                                while($dataRow1=mysqli_fetch_assoc($result1)){ ?>
                                                <a class="dropdown-item" href="crafts?id=<?php echo $dataRow1['sub_cat_id'] ?>"><?php echo $dataRow1['subcat_name'] ?></a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        <?php }else{ ?>
                            <li class="nav-item">
                                <a class="nav-link" href="craft?id=<?php echo $dataRow['category_id'] ?>"><?php echo $dataRow['category_name'] ?></a>
                            </li>
                        <?php }
                    } ?>
                    
                    
                    
                </ul>

            </div>
        </nav>
        <!-- //bottom nav -->
    </div>
    <!-- //header container -->
</header>

<!-- sign up Modal -->
<div class="modal fade" id="myModal_btn" tabindex="-1" role="dialog" aria-labelledby="myModal_btn" aria-hidden="true">
    <div class="agilemodal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Register Now</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pt-3 pb-5 px-sm-5">
                <div class="row">
                    <div class="col-md-6 mx-auto align-self-center">
                        <img src="images/p3.png" class="img-fluid" alt="login_image" />
                    </div>
                    <div class="col-md-6">
                        <form action="#" onsubmit="signUp();return false">
                            <div class="form-group">
                                <label for="recipient-name1" class="col-form-label">Your Name</label>
                                <input type="text" class="form-control" placeholder=" " name="Name" id="recipient-name1"
                                    required="">
                            </div>
                            <div class="form-group">
                                <label for="recipient-email" class="col-form-label">Email</label>
                                <input type="email" class="form-control" placeholder=" " name="Email"
                                    id="recipient-email" required="">
                            </div>
                            <div class="form-group">
                                <label for="password1" class="col-form-label">Password</label>
                                <input type="password" class="form-control" placeholder=" " name="Password"
                                    id="password1" required="">
                            </div>
                            <div class="form-group">
                                <label for="password2" class="col-form-label">Confirm Password</label>
                                <input type="password" class="form-control" placeholder=" " name="Confirm Password"
                                    id="password2" required="">
                            </div>
                            <div class="sub-w3l">
                                <div class="sub-agile">
                                    <label for="brand2" class="mb-3" style="color:red;display:none;"
                                        id="signUpError"></label>
                                </div>
                            </div>
                            <div class="right-w3l">
                                <input type="submit" class="form-control" value="Register">
                            </div>
                        </form>
                        <p class="text-center mt-3">Already a member?
                            <a href="#" data-toggle="modal" data-target="#exampleModal1" class="text-dark login_btn">
                                Login Now</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- //signup modal -->
<!-- signin Modal -->
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModal1"
    aria-hidden="true">
    <div class="agilemodal-dialog modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body  pt-3 pb-5 px-sm-5">
                <div class="row">
                    <div class="col-md-6 align-self-center">
                        <img src="images/p3.png" class="img-fluid" alt="login_image" />
                    </div>
                    <div class="col-md-6">
                        <form action="#" onsubmit="login();return false">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">Your Email</label>
                                <input type="email" class="form-control" placeholder=" " name="Name" id="loginEmail1"
                                    required="">
                            </div>
                            <div class="form-group">
                                <label class="col-form-label">Password</label>
                                <input type="password" class="form-control" placeholder=" " name="Password" required=""
                                    id="loginpwd">
                            </div>
                            <div class="sub-w3l">
                                <div class="sub-agile">
                                    <label for="brand2" class="mb-3" style="color:red;display:none;"
                                        id="loginError"></label>
                                </div>
                            </div>
                            <div class="right-w3l">
                                <input type="submit" class="form-control" value="Login">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- signin Modal -->

<!-- script for password match -->
<script>
window.onload = function() {
    document.getElementById("password1").onchange = validatePassword;
    document.getElementById("password2").onchange = validatePassword;
}

function validatePassword() {
    var pass2 = document.getElementById("password2").value;
    var pass1 = document.getElementById("password1").value;
    if (pass1 != pass2)
        document.getElementById("password2").setCustomValidity("Passwords Don't Match");
    else
        document.getElementById("password2").setCustomValidity('');
    //empty string means no validation error
}

/**
 * sign up function
 */
function signUp() {
    var signName = document.getElementById("recipient-name1").value;
    var signEmail = document.getElementById("recipient-email").value;
    var signpwd1 = document.getElementById("password1").value;

    obj = {
        "name": signName,
        "email": signEmail,
        "password": signpwd1
    }

    $.ajax({
        url: "ajax/signup.php",
        type: "POST",
        data: {
            data: obj
        },

        success: function(data) {
            var res = JSON.parse(data);

            if (res.status == 1) {
                document.getElementById("signUpError").style.display = "block";
                document.getElementById('signUpError').innerHTML = 'User with this email already exists!';
            } else if (res.status == 3) {
                document.getElementById("signUpError").style.display = "block";
                document.getElementById('signUpError').innerHTML = 'Register Not Success!';
            } else {
                location.reload();
            }
        },
        error: function(xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert(errorMessage);
        }
    });
}

/**
 * login function
 */
function login() {
    var loginEmail = document.getElementById("loginEmail1").value;
    var loginpwd = document.getElementById("loginpwd").value;

    obj = {
        "email": loginEmail,
        "password": loginpwd
    }

    $.ajax({
        url: "ajax/login.php",
        type: "POST",
        data: {
            data: obj
        },

        success: function(data) {
            var res = JSON.parse(data);

            if (res.status == 1) {
                document.getElementById("loginError").style.display = "block";
                document.getElementById('loginError').innerHTML = 'User with that email does not exist !';
            } else if (res.status == 3) {
                document.getElementById("loginError").style.display = "block";
                document.getElementById('loginError').innerHTML =
                    'You have entered wrong password, try again !';
            } else {
                location.reload();
            }
        },
        error: function(xhr, status, error) {
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert(errorMessage);
        }
    });
}
</script>