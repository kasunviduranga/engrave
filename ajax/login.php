<?php 
include('../db/dbConnection.php');
session_start();

$data = (array) $_POST['data'];

$email = $mysqli->escape_string($data['email']);
$password =$data['password'];

$result = $mysqli->query("SELECT * FROM  user_tbl WHERE user_email='$email'");

if ( $result->num_rows == 0 ){ // User doesn't exist
  $_SESSION['message'] = "User with that email doesn't exist!";
  $_SESSION['icon'] = 1;
  
}else { // User exists
    $user = $result->fetch_assoc();
    if (password_verify($password, $user['user_pwd']) ){
        $_SESSION['user_email'] = $user['user_email'];
        $_SESSION['user_name'] = $user['user_name'];
        $_SESSION['user_id'] = $user['user_id'];

        $_SESSION['icon'] = 2;
        $_SESSION['message'] = "login successfully";
    }else {
        $_SESSION['message'] = "You have entered wrong password, try again!";
        $_SESSION['icon'] = 3;    
    }
}

$response_array['status'] = $_SESSION['icon'];
echo json_encode($response_array);

?>