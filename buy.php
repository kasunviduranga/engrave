<?php 
include 'db/dbConnection.php';
$id = $_GET['id'];

$sql = "SELECT * FROM `product_tbl` WHERE pro_id= $id";
$result = mysqli_query($connection, $sql);
while ($dataRow = mysqli_fetch_assoc($result)) {
    $name = $dataRow['pro_name'];
    $prise = $dataRow['pro_prise'];
    $color = $dataRow['pro_color'];
    $description = $dataRow['pro_description'];
    $fabric = $dataRow['pro_fabrick'];
    $weight = $dataRow['pro_weight'];
    $material = $dataRow['pro_material'];
    $size = $dataRow['pro_size'];
    $deliver = $dataRow['pro_delive'];
    $message = $dataRow['pro_message'];
    $image = $dataRow['pro_image'];
    $font = $dataRow['pro_font'];
} ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <title><?php echo $name ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="" />
    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <!-- shop css -->
    <link href="css/shop.css" type="text/css" rel="stylesheet" media="all">
    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- <link rel="stylesheet" href="css/owl.theme.default.min.css"> -->
    <!-- Owl-Carousel-CSS -->
    <!-- Owl-Carousel-CSS -->
    <!-- <link rel="stylesheet" href="css/owl.carousel.css" type="text/css" media="all"> -->
    <!-- flexslider-css -->
    <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />
    <!-- shop css -->
    <link href="css/shop.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/checkout.css" type="text/css" rel="stylesheet" media="all">
    <!-- easy-responsive-tabs css -->
    <link rel="stylesheet" href="css/easy-responsive-tabs.css" type="text/css" media="all" />
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
        rel="stylesheet">
    <!-- //online-fonts -->

    <!-- font style -->
    <link href='https://fonts.googleapis.com/css?family=Sofia' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Adamina' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Alegreya SC' rel='stylesheet'>
    <link href='https://fonts.googleapis.com/css?family=Aladin' rel='stylesheet'>

</head>

<body>
    <!-- header -->
    <?php include 'header.php' ?>
    <!-- //header -->
    <!-- inner banner -->
    <div class="ibanner_w3 pt-sm-5 pt-3">
        <h4 class="head_agileinfo text-center text-capitalize text-center pt-5">
            <span>e</span>ngrave.
            <span>l</span>k</h4>
    </div>
    <!-- //inner banner -->
    <!-- breadcrumbs -->
    <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index.html">Home</a>
            </li>
            <li class="breadcrumb-item">
                <a href="boys.html">Boy's Clothing</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Single Product</li>
        </ol>
    </nav> -->
    <!-- //breadcrumbs -->
    <!-- Single -->
    <div class="innerf-pages section">
        <div class="container">
            <div class="row my-sm-5">
                <div class="col-lg-4 single-right-left">
                    <div class="grid images_3_of_2">
                        <div class="flexslider1">
                            <ul class="slides">
                            <?php
                            $sql ="SELECT * FROM `image_table` WHERE pro_id = $id limit 3";
                            $result = mysqli_query($connection,$sql);
                            while($dataRow=mysqli_fetch_assoc($result)){
                            ?>
                                <li data-thumb="admin/galleryImg/<?php echo $dataRow['image_name'] ?>">
                                    <div class="thumb-image">
                                        <img src="admin/galleryImg/<?php echo $dataRow['image_name'] ?>"
                                            data-imagezoom="true" alt=" " class="img-fluid">
                                    </div>
                                </li>
                            <?php } ?>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 mt-lg-0 mt-5 single-right-left simpleCart_shelfItem">
                    <h3><?php echo $name ?></h3>
                    <div class="caption">
                        <ul class="rating-single">
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <span class="fa fa-star yellow-star" aria-hidden="true"></span>
                                </a>
                            </li>
                        </ul>
                        <div class="clearfix"> </div>
                        <h6>Rs. <?php echo number_format($prise,2) ?></h6>
                    </div>
                    <div class="desc_single">
                        <h5>Description</h5>
                        <p><?php echo $description ?></p>
                    </div>
                    <div class="d-sm-flex justify-content-between">
                        <div class="occasional">
                            <h5 class="sp_title mb-3">Highlights</h5>
                            <ul class="single_specific">
                                <li>
                                    <span> Fabric: </span> <?php echo $fabric ?>
                                </li>
                                <li>
                                    <span>Color :</span> <?php echo $color ?>
                                </li>
                                
                            </ul>

                        </div>
                        <div class="color-quality mt-sm-0 mt-4">
                            <h5 class="sp_title mb-3">services</h5>
                            <ul class="single_serv">
                                <li>
                                    <a href="#">30 Day Return Policy</a>
                                </li>
                                <li class="mt-2">
                                    <a href="#">Cash on Delivery available</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php if($message == 1){ ?>
                    <div class="description">
                        <h5>Add Your Message: <span style="color:red;font-size:20px;">*</span></h5>
                        <textarea style="width: 100%;" id="writeText"></textarea>
                    </div>
                    <?php } ?>
                    <?php if($font == 1){ ?>
                    <div class="description">
                        <h5>Select Font: <span style="color:red;font-size:20px;">*</span></h5>
                        <select id="cars" style="width: 50%;">
                            <option value="volvo" style="font-family: 'Sofia';">Select Font</option>
                            <option value="saab" style="font-family: 'Adamina';">Select Font</option>
                            <option value="vw" style="font-family: 'Aladin';">Select Font</option>
                            <option value="audi" style="font-family: 'Alegreya SC';">Select Font</option>
                        </select>
                    </div>
                    <?php } ?>
                    <?php if($image == 1){ ?>
                    <div class="description">
                        <h5>Add Your Image: <span style="color:red;font-size:20px;">*</span></h5>
                        <input type="file" style="width: 100%;" id="imageUpload"></input>
                    </div>
                    <?php } ?>
                    <div class="occasion-cart" style="margin: auto;bottom: 0;position: absolute;">
                        <div class="chr single-item single_page_b">
                            <button type="submit" class="hub-cart phub-cart btn" onclick="addToCart(<?php echo $id ?>)">
                                <i class="fa fa-cart-plus" aria-hidden="true"> add to cart</i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Payment-->
    <section class="payment_w3ls py-5">
        <div class="container">
            <div class="privacy about">
                <h5 class="head_agileinfo text-center text-capitalize pb-0">
                    <span>D</span>etails</h5>
                <!--/tabs-->
                <div class="responsive_tabs innfpage-tabs">
                    <div id="horizontalTab">
                        <ul class="resp-tabs-list">
                            <li>Product Description</li>
                            <li> Features & Shipping</li>
                            <li>Reviews</li>
                            <li>Add Review</li>
                        </ul>
                        <div class="resp-tabs-container">
                            <!--/tab_one-->
                            <div class="tab1">
                                <div class="pay_info">
                                    <div class="vertical_post check_box_fpay">
                                        <span>About the Product</span>
                                        <br>
                                        <p><?php echo $description ?></p>
                                        <br>
                                        <p>Weight: <?php echo $weight ?></p>
                                        <br>
                                        <p>Delivered in <?php echo $deliver ?></p>
                                    </div>
                                </div>
                            </div>
                            <!--//tab_one-->
                            <div class="tab2">
                                <div class="pay_info">
                                    <div class="vertical_post check_box_fpay">
                                        <!-- <span>Shipping Cost (in Rs.)</span><span style="margin-left:30px;">50</span> -->
                                        <!-- <hr> -->
                                        <span>Standard Delivery</span><span style="margin-left:30px;">Delivered in <?php echo $deliver ?></span>
                                        <hr>
                                        <span>Material</span><span style="margin-left:30px;"><?php echo $material ?></span>
                                        <hr>
                                        <span>Size</span><span style="margin-left:30px;"><?php echo $size ?></span>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                            <div class="tab3">

                                <div class="pay_info">

                                </div>
                            </div>
                            <div class="tab4">
                                <div class="pay_info">
                                    <div class="vertical_post check_box_fpay">
                                        <h5>WRITE YOUR OWN REVIEW</h5>
                                        <br>
                                        <span>Only registered users can write reviews. Please, log in or register</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--//tabs-->
            </div>

        </div>
    </section>
    <!-- //payment -->
    <?php include 'footer.php' ?>
    <!-- //footer -->

    <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <script>
    function addToCart(pro_id) {
        
        var fd = new FormData();

        var imageFile = $('#imageUpload')[0].files[0];
        var writeText = $('#writeText').val();

        fd.append('imageFile', imageFile);
        fd.append('writeText', writeText);
        fd.append('product', pro_id);

        console.log(fd);

        $.ajax({
            url: "ajax/checkout.php",
            type: "POST",
            data:fd,
            dataType:'json',
            contentType:false,
            processData:false,
            cash:false,

            success: function(data) {
                console.log(data.status);
                // var res = JSON.parse(data);

                if (data.status == 1) {
                    window.location.href="checkout";
                } else if (data.status == 3) {
                    $('#exampleModal1').modal('show');
                } else {
                    alert("Cart To Added not success !");
                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert(errorMessage);
            }
        });
    }
    </script>
    <!-- smooth dropdown -->
    <script>
    $(document).ready(function() {
        $('ul li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        });
    });
    </script>
    <!-- //smooth dropdown -->

    <script src="js/minicart.js"></script>
    <script>
    hub.render();

    hub.cart.on('new_checkout', function(evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
    });
    </script>
    <!-- //cart-js -->
    <!-- easy-responsive-tabs -->
    <script src="js/easy-responsive-tabs.js"></script>
    <script>
    $(document).ready(function() {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion           
            width: 'auto', //auto or any width like 600px
            fit: true, // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
    });
    </script>
    <!-- //easy-responsive-tabs -->
    <!-- FlexSlider -->
    <script src="js/jquery.flexslider.js"></script>
    <script>
    // Can also be used with $(document).ready()
    $(window).load(function() {
        $('.flexslider1').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
        });
    });
    </script>
    <!-- //FlexSlider-->
    <!-- Responsiveslides -->
    <script src="js/responsiveslides.min.js"></script>
    <script>
    // You can also use "$(window).load(function() {"
    $(function() {
        // Slideshow 4
        $("#slider3").responsiveSlides({
            auto: false,
            pager: true,
            nav: false,
            speed: 500,
            namespace: "callbacks",
            before: function() {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function() {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
    </script>
    <!-- // Responsiveslides -->
    <!-- cart-js -->
    <script src="js/minicart.js"></script>
    <script>
    hub.render();

    hub.cart.on('new_checkout', function(evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
    });
    </script>
    <!-- //cart-js -->
    <!-- credit-card -->
    <script src="js/creditly.js"></script>
    <link rel="stylesheet" href="css/creditly.css" type="text/css" media="all" />

    <script>
    $(function() {
        var creditly = Creditly.initialize(
            '.creditly-wrapper .expiration-month-and-year',
            '.creditly-wrapper .credit-card-number',
            '.creditly-wrapper .security-code',
            '.creditly-wrapper .card-type');

        $(".creditly-card-form .submit").click(function(e) {
            e.preventDefault();
            var output = creditly.validate();
            if (output) {
                // Your validated credit card output
                console.log(output);
            }
        });
    });
    </script>
    <!-- //credit-card -->
    <!-- zoom -->
    <script src="js/imagezoom.js"></script>
    <!-- zoom-->

    <!-- start-smooth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
    $(document).ready(function() {
        /*
        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
        };
        */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
    </script>
    <!-- <script src="js/SmoothScroll.min.js"></script> -->
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>

</html>