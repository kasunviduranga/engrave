<?php
include 'db/dbConnection.php';
session_start();

if (!isset($_SESSION['user_id'])) {
    header('Location: index');
}
?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Fashion Hub Ecommerce Category Bootstrap Responsive Website Template| Checkout :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="" />
    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <!-- shop css -->
    <link href="css/shop.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/checkout.css" type="text/css" rel="stylesheet" media="all">
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
        rel="stylesheet">
    <!-- //online-fonts -->
</head>

<body>
    <!-- header -->
    <?php include 'header.php'; ?>
    <!-- //header -->
    <!-- inner banner -->
    <div class="ibanner_w3 pt-sm-5 pt-3">
        <h4 class="head_agileinfo text-center text-capitalize text-center pt-5">
            <span>e</span>ngrave.
            <span>l</span>k</h4>
    </div>
    <!-- //inner banner -->
    <!-- breadcrumbs -->
    <!-- <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item">
                <a href="index">Home</a>
            </li>
            <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
    </nav> -->
    <!-- //breadcrumbs -->
    <!--checkout-->
    <section class="checkout_wthree py-sm-5 py-3">
        <div class="container">
            <div class="check_w3ls">
                <div class="d-sm-flex justify-content-between mb-4">
                    <h4>review your order
                    </h4>
                    <h4 class="mt-sm-0 mt-3">Your shopping cart contains:
                        <span>3 Products</span>
                    </h4>
                </div>
                <div class="checkout-right">
                    <table class="timetable_sub">
                        <thead>
                            <tr>
                                <th>SL No.</th>
                                <th>Product</th>
                                <th>Quantity</th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Remove</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                        $x = 1;
                        $sql ="SELECT * FROM `user_checkout`,`product_tbl` WHERE user_checkout.check_user = $_SESSION[user_id] AND product_tbl.pro_id = user_checkout.check_pro";
                        $result = mysqli_query($connection,$sql);
                        while($dataRow=mysqli_fetch_assoc($result)){
                            $sql1 ="SELECT * FROM `image_table` WHERE image_table.pro_id = $dataRow[pro_id] limit 1";
                            $result1 = mysqli_query($connection,$sql1);
                            while($dataRow1=mysqli_fetch_assoc($result1)){
                                $image = $dataRow1['image_name'];
                            }
                            
                        ?>
                            <tr class="rem<?php echo $x?>">
                                <td class="invert"><?php echo $x?></td>
                                <td class="invert-image">
                                    <a href="single_product.html">
                                        <img src="admin/galleryImg/<?php echo $image?>"
                                            style="width: 140px;height: auto;" alt=" " class="img-responsive">
                                    </a>
                                </td>
                                <td class="invert">
                                    <div class="quantity">
                                        <div class="quantity-select">
                                            <div class="entry value-minus">&nbsp;</div>
                                            <input type="hidden" class="checkoutId"
                                                value="<?php echo $dataRow['check_id']?>">
                                            <div class="entry value">
                                                <span
                                                    id="qtySpan<?php echo $dataRow['check_id']?>"><?php echo $dataRow['check_qty']?></span>
                                            </div>
                                            <div class="entry value-plus active">&nbsp;</div>
                                        </div>
                                    </div>
                                </td>
                                <td class="invert"><?php echo $dataRow['pro_name']?></td>

                                <td class="invert">Rs.<?php echo number_format($dataRow['pro_prise'],2) ?></td>
                                <td class="invert">
                                    <div class="rem">
                                        <div class="" style="color:red;cursor:pointer;"
                                            onclick="deleteCheckout(<?php echo $dataRow['check_id']?>)"><i
                                                class="far fa-trash-alt"></i></div>
                                    </div>

                                </td>
                            </tr>
                            <?php $x++; } ?>
                        </tbody>
                    </table>
                </div>
                <div class="row checkout-left mt-5">
                    <div class="col-md-4 checkout-left-basket">
                        <h4>Continue to basket</h4>
                        <ul>
                            <?php
                        $x = 1;
                        $total = 0;
                        $sql ="SELECT * FROM `user_checkout`,`product_tbl` WHERE user_checkout.check_user = $_SESSION[user_id] AND product_tbl.pro_id = user_checkout.check_pro";
                        $result = mysqli_query($connection,$sql);
                        while($dataRow=mysqli_fetch_assoc($result)){
                            $total += $dataRow['pro_prise']*$dataRow['check_qty'] ;
                        ?>
                            <li><?php echo $dataRow['pro_name']?>
                                <i> -</i>
                                <span>Rs.<?php echo number_format($dataRow['pro_prise']*$dataRow['check_qty'],2) ?>
                                </span>
                            </li>
                            <?php } ?>

                            <li class="totalCheckout" style="color:black !important;">Total
                                <i> -</i>
                                <span>Rs.<?php echo number_format($total,2) ?></span>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-8 address_form">
                        <h4>Billing Address</h4>
                        <form action="payment" method="post" class="creditly-card-form shopf-sear-headinfo_form">
                            <div class="creditly-wrapper wrapper">
                                <div class="information-wrapper">
                                    <div class="first-row form-group">
                                        <div class="controls">
                                            <label class="control-label">Full name: </label>
                                            <input class="billing-address-name form-control" type="text" name="name"
                                                placeholder="Full name">
                                        </div>
                                        <div class="card_number_grids">
                                            <div class="card_number_grid_left">
                                                <div class="controls">
                                                    <label class="control-label">Mobile number:</label>
                                                    <input class="form-control" type="text" placeholder="Mobile number">
                                                </div>
                                            </div>
                                            <div class="card_number_grid_right">
                                                <div class="controls">
                                                    <label class="control-label">Landmark: </label>
                                                    <input class="form-control" type="text" placeholder="Landmark">
                                                </div>
                                            </div>
                                            <div class="clear"> </div>
                                        </div>
                                        <div class="controls">
                                            <label class="control-label">Town/City: </label>
                                            <input class="form-control" type="text" placeholder="Town/City">
                                        </div>
                                        <div class="controls">
                                            <label class="control-label">Address type: </label>
                                            <select class="form-control option-fieldf">
                                                <option>Office</option>
                                                <option>Home</option>
                                                <option>Commercial</option>

                                            </select>
                                        </div>
                                    </div>
                                    <button class="submit check_out">place order</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--//checkout-->
    <?php include 'footer.php'; ?>
    <!-- //footer -->

    <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <!-- smooth dropdown -->
    <script>
    $(document).ready(function() {
        $('ul li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        });
    });
    </script>
    <!-- //smooth dropdown -->
    <!-- script for password match -->
    <script>
    function deleteCheckout(checkoutId) {

        obj = {
            "id": checkoutId
        }

        $.ajax({
            url: "ajax/deleteCheckout.php",
            type: "POST",
            data: {
                data: obj
            },

            success: function(data) {
                var res = JSON.parse(data);

                if (res.status == 2) {
                    alert("Not Success !");
                } else {
                    location.reload();
                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert(errorMessage);
            }
        });
    }
    </script>
    <!--quantity-->
    <script>
    $('.value-plus').on('click', function() {

        var divUpd = $(this).parent().find('.value'),
            newVal = parseInt(divUpd.text(), 10) + 1;
        divUpd.text(newVal);
        var checkoutId = $(this).parent().find('.checkoutId').val();


        obj = {
            "checkoutId": checkoutId,
            "qty": newVal
        }

        $.ajax({
            url: "ajax/editQty.php",
            type: "POST",
            data: {
                data: obj
            },

            success: function(data) {
                var res = JSON.parse(data);

                if (res.status == 2) {
                    alert("QTY Update not Success !");
                } else {
                    location.reload();
                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert(errorMessage);
            }
        });
    });

    $('.value-minus').on('click', function() {
        var divUpd = $(this).parent().find('.value'),
            newVal = parseInt(divUpd.text(), 10) - 1;
        if (newVal >= 1) {
            divUpd.text(newVal);
            var checkoutId = $(this).parent().find('.checkoutId').val();

            obj = {
                "checkoutId": checkoutId,
                "qty": newVal
            }

            $.ajax({
                url: "ajax/editQty.php",
                type: "POST",
                data: {
                    data: obj
                },

                success: function(data) {
                    var res = JSON.parse(data);

                    if (res.status == 2) {
                        alert("QTY Update not Success !");
                    } else {
                        location.reload();
                    }
                },
                error: function(xhr, status, error) {
                    var errorMessage = xhr.status + ': ' + xhr.statusText;
                    alert(errorMessage);
                }
            });
        }
    });
    </script>
    <!--quantity-->
    <!-- script for password match -->
    <!-- cart-js -->
    <script src="js/minicart.js"></script>
    <script>
    hub.render();

    hub.cart.on('new_checkout', function(evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
    });
    </script>
    <!-- //cart-js -->

    <!-- FadeOut-Script -->
    <script>
    $(document).ready(function(c) {
        $('.close1').on('click', function(c) {
            $('.rem1').fadeOut('slow', function(c) {
                $('.rem1').remove();
            });
        });
    });
    </script>
    <script>
    $(document).ready(function(c) {
        $('.close2').on('click', function(c) {
            $('.rem2').fadeOut('slow', function(c) {
                $('.rem2').remove();
            });
        });
    });
    </script>
    <script>
    $(document).ready(function(c) {
        $('.close3').on('click', function(c) {
            $('.rem3').fadeOut('slow', function(c) {
                $('.rem3').remove();
            });
        });
    });
    </script>
    <!--// FadeOut-Script -->

    <!-- start-smooth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
    $(document).ready(function() {
        /*
        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
        };
        */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
    </script>
    <!-- <script src="js/SmoothScroll.min.js"></script> -->
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>

</html>