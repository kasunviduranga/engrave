<?php include 'db/dbConnection.php';?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title>Fashion Hub Ecommerce Category Bootstrap Responsive Website Template| Home :: w3layouts</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Fashion Hub Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
    addEventListener("load", function() {
        setTimeout(hideURLbar, 0);
    }, false);

    function hideURLbar() {
        window.scrollTo(0, 1);
    }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <!-- shop css -->
    <link href="css/shop.css" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Owl-Carousel-CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
        rel="stylesheet">
    <!-- //online-fonts -->
</head>

<body>
    <!-- header -->
    <?php include 'header.php'; ?>
    <!-- //header -->
    <!-- banner -->
    <div class="banner-text">
        <div class="callbacks_container">
            <ul class="rslides" id="slider3">
                <li class="banner">
                    <div class="container">
                        <h3 class="agile_btxt">
                            <span>E</span>ngrave.
                            <span>l</span>k
                        </h3>
                        <!-- <h4 class="w3_bbot">shop exclusive clothing</h4> -->
                        <!-- <div class="slider-info mt-sm-5">
                            <h4 class="bn_right">
                                <span>w</span>omen's
                                <span>f</span>ashion</h4>
                            <div class="bnr_clip position-relative">
                                <h4>get up to
                                    <span class="px-2">45% </span>
                                </h4>
                                <div class="d-inline-flex flex-column banner-pos position-absolute text-center">
                                    <div class="bg-flex-item">
                                        <span>O</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F
                                        </span>
                                    </div>
                                </div>
                                <p class="text-uppercase py-2">on special sale</p>
                                <a class="btn btn-primary mt-3 text-capitalize" href="shop.html" role="button">shop
                                    now</a>
                            </div>
                        </div> -->
                    </div>
                </li>
                <li class="banner banner2">
                    <div class="container">
                        <h3 class="agile_btxt">
                            <span>E</span>ngrave.
                            <span>l</span>k
                        </h3>
                        <!-- <h4 class="w3_bbot">shop exclusive clothing</h4> -->
                        <!-- <div class="slider-info mt-sm-5">
                            <h4 class="bn_right">
                                <span>m</span>en's
                                <span>f</span>ashion</h4>
                            <div class="bnr_clip position-relative">
                                <h4>get up to
                                    <span class="px-2">35% </span>
                                </h4>
                                <div class="d-inline-flex flex-column banner-pos position-absolute text-center">
                                    <div class="bg-flex-item">
                                        <span>O</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F
                                        </span>
                                    </div>
                                </div>
                                <p class="text-uppercase py-2">on special sale</p>
                                <a class="btn btn-primary mt-3 text-capitalize" href="shop.html" role="button">shop
                                    now</a>
                            </div>
                        </div> -->
                    </div>
                </li>
                <li class="banner banner3">
                    <div class="container">
                        <h3 class="agile_btxt">
                            <span>E</span>ngrave.
                            <span>l</span>k
                        </h3>
                        <!-- <h4 class="w3_bbot">shop exclusive clothing</h4> -->
                        <!-- <div class="slider-info mt-sm-5">
                            <h4 class="bn_right">
                                <span>k</span>id's
                                <span>f</span>ashion</h4>
                            <div class="bnr_clip position-relative">
                                <h4>get up to
                                    <span class="px-2">45% </span>
                                </h4>
                                <div class="d-inline-flex flex-column banner-pos position-absolute text-center">
                                    <div class="bg-flex-item">
                                        <span>O</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F</span>
                                    </div>
                                    <div class="bg-flex-item">
                                        <span>F
                                        </span>
                                    </div>
                                </div>
                                <p class="text-uppercase py-2">on special sale</p>
                                <a class="btn btn-primary mt-3 text-capitalize" href="shop.html" role="button">shop
                                    now</a>
                            </div>
                        </div> -->
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- //banner -->
    
    <!-- product tabs -->
    <section class="tabs_pro py-md-5 pt-sm-3 pb-5">
        <h5 class="head_agileinfo text-center text-capitalize pb-5">
            <span>N</span>ew Finds</h5>
        <div class="tabs tabs-style-line pt-md-5">
            <nav class="container">
                <ul id="clothing-nav" class="nav nav-tabs tabs-style-line" role="tablist">
                    <li class="nav-item">
                        <?php
                        $sql="SELECT * FROM category_tbl WHERE category_id=1";
                        $res=mysqli_query($connection,$sql);
                        $data=mysqli_fetch_array($res);
                        ?>
                        <a class="nav-link active" href="#women" id="women-tab" role="tab" data-toggle="tab"
                           aria-controls="women" aria-expanded="true"><?php echo $data['category_name']; ?></a>
                    </li>

                    <li class="nav-item">
                        <?php
                        $sql="SELECT * FROM category_tbl WHERE category_id=2";
                        $res=mysqli_query($connection,$sql);
                        $data=mysqli_fetch_array($res);
                        ?>
                        <a class="nav-link" href="#men" role="tab" id="men-tab" data-toggle="tab"
                           aria-controls="men"><?php echo $data['category_name']; ?>
                        </a>
                    </li>
                    <li class="nav-item">
                        <?php
                        $sql="SELECT * FROM category_tbl WHERE category_id=3";
                        $res=mysqli_query($connection,$sql);
                        $data=mysqli_fetch_array($res);
                        ?>
                        <a class="nav-link" href="#girl" role="tab" id="girl-tab" data-toggle="tab"
                           aria-controls="girl"><?php echo $data['category_name']; ?></a>
                    </li>
                    <li class="nav-item">
                        <?php
                        $sql="SELECT * FROM category_tbl WHERE category_id=4";
                        $res=mysqli_query($connection,$sql);
                        $data=mysqli_fetch_array($res);
                        ?>
                        <a class="nav-link" href="#boy" role="tab" id="boy-tab" data-toggle="tab"
                           aria-controls="boy"><?php echo $data['category_name'];?></a>
                    </li>
                </ul>
            </nav>
            <!-- Content Panel -->
            <div id="clothing-nav-content" class="tab-content py-sm-5">
                <div role="tabpanel" class="tab-pane fade show active" id="women" aria-labelledby="women-tab">
                    <div id="owl-demo" class="owl-carousel text-center">
                        <?php
                        $sql="SELECT * FROM `product_tbl` INNER JOIN `image_table` ON product_tbl.pro_id=image_table.pro_id WHERE product_tbl.pro_catid=1 ";
                        $res=mysqli_query($connection,$sql);
                        ?>
                        <?php while ($data=mysqli_fetch_array($res)){?>
                            <div class="item">
                                <div class="card product-men p-3">
                                    <div class="men-thumb-item">
                                        <img style="height: 250px; width: 250px" src="galleryImg/<?php echo$data['image_name'];?>" alt="img" class="card-img-top">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="buy?id=<?php echo $data['pro_id'];?>" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body  py-3 px-2">
                                        <h5 class="card-title text-capitalize"><?php echo $data['pro_name'];?></h5>
                                        <div class="d-flex justify-content-between">
                                            <p class="font-weight-bold">Rs. <?php echo number_format($data['pro_prise'],2) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="men" aria-labelledby="men-tab">
                    <div id="owl-demo1" class="owl-carousel text-center">
                        <?php
                        $sql="SELECT * FROM `product_tbl` INNER JOIN `image_table` ON product_tbl.pro_id=image_table.pro_id WHERE product_tbl.pro_catid=2 ";
                        $res=mysqli_query($connection,$sql);
                        ?>
                        <?php while ($data=mysqli_fetch_array($res)){?>
                            <div class="item">
                                <div class="card product-men p-3">
                                    <div class="men-thumb-item">
                                        <img style="height: 250px; width: 250px" src="galleryImg/<?php echo $data['image_name'];?>" alt="img" class="card-img-top">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="buy?id=<?php echo $data['pro_id'];?>" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body  py-3 px-2">
                                        <h5 class="card-title text-capitalize"><?php echo $data['pro_name'];?></h5>
                                        <div class="d-flex justify-content-between">
                                            <p class="text-dark font-weight-bold">Rs. <?php echo number_format($data['pro_prise'],2) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="girl" aria-labelledby="girl-tab">
                    <div id="owl-demo2" class="owl-carousel text-center">
                        <?php
                        $sql="SELECT * FROM `product_tbl` INNER JOIN `image_table` ON product_tbl.pro_id=image_table.pro_id WHERE product_tbl.pro_catid=3 ";
                        $res=mysqli_query($connection,$sql);
                        ?>
                        <?php while ($data=mysqli_fetch_array($res)){?>
                            <div class="item">
                                <div class="card product-men p-3">
                                    <div class="men-thumb-item">
                                        <img style="height: 250px; width: 250px" src="galleryImg/<?php echo$data['image_name'];?>" alt="img" class="card-img-top">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="buy?id=<?php echo $data['pro_id'];?>" class="link-product-add-cart">Quick Viewxxx</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body  py-3 px-2">
                                        <h5 class="card-title text-capitalize"><?php echo $data['pro_name'];?></h5>
                                        <div class="d-flex justify-content-between">
                                            <p class="text-dark font-weight-bold">Rs. <?php echo number_format($data['pro_prise'],2) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                    </div>
                </div>

                <div role="tabpanel" class="tab-pane fade" id="boy" aria-labelledby="boy-tab">
                    <div id="owl-demo3" class="owl-carousel text-center">

                        <?php
                        $sql="SELECT * FROM `product_tbl` INNER JOIN `image_table` ON product_tbl.pro_id=image_table.pro_id WHERE product_tbl.pro_catid=4 ";
                        $res=mysqli_query($connection,$sql);
                        ?>
                        <?php while ($data=mysqli_fetch_array($res)){?>
                            <div class="item">
                                <div class="card product-men p-3">
                                    <div class="men-thumb-item">
                                        <img style="height: 250px; width: 250px" src="galleryImg/<?php echo$data['image_name'];?>" alt="img" class="card-img-top">
                                        <div class="men-cart-pro">
                                            <div class="inner-men-cart-pro">
                                                <a href="buy?id=<?php echo $data['pro_id'];?>" class="link-product-add-cart">Quick View</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body  py-3 px-2">
                                        <h5 class="card-title text-capitalize"><?php echo $data['pro_name'];?></h5>
                                        <div class="d-flex justify-content-between">
                                            <p class="text-dark font-weight-bold">Rs. <?php echo number_format($data['pro_prise'],2) ?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>

                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- //product tabs -->
    <!-- insta posts -->
    <section class="py-lg-5">
        <!-- insta posts -->
        <h5 class="head_agileinfo text-center text-capitalize pb-5">
            <span>T</span>rending Items</h5>
        <div class="gallery row no-gutters pt-lg-5">
            
            <?php
            $sql = 'SELECT * FROM `product_tbl` limit 12';
            $result = mysqli_query($connection, $sql);
            while ($dataRow = mysqli_fetch_assoc($result)) {

                $image = "" ;
                $sql1 ="SELECT * FROM `image_table` WHERE pro_id = $dataRow[pro_id] limit 1";
                $result1 = mysqli_query($connection,$sql1);
                while($dataRow1=mysqli_fetch_assoc($result1)){
                    $image = $dataRow1['image_name'] ;
                }
            ?>

            <div class="col-lg-2 col-sm-4 col-6 gallery-item">
                <img src="admin/galleryImg/<?php echo $image ?>" class="img-fluid" alt="" />
                <div class="gallery-item-info">
                    <ul>
                        <a href="buy?id=<?php echo $dataRow['pro_id'];?>" style="color:white;">
                            <li class="gallery-item-likes">
                            <i class="fas fa-eye"></i> View</li>
                        </a>
                    </ul>
                </div>
            </div>

            <?php } ?>
            
        </div>
        <!-- <div class="gallery row no-gutters pb-5">
            
            <div class="col-lg-2 col-sm-4 col-6 gallery-item">
                <img src="images/image13.gif" class="img-fluid" alt="" />
                <div class="gallery-item-info">
                    <ul>
                        <li class="gallery-item-likes">
                            <i class="fas fa-heart"></i> 56</li>
                        <li class="gallery-item-comments">
                            <i class="fas fa-comment"></i> 2</li>
                    </ul>
                </div>
            </div>
            
        </div> -->
    </section>
    <!-- //insta posts -->
    <!-- footer -->
    <?php include 'footer.php'; ?>
    <!-- //footer -->
    
    <!-- js -->
    <script src="js/jquery-2.2.3.min.js"></script>
    <!-- //js -->
    <!-- script for show signin and signup modal -->
    <!-- <script>
    $(document).ready(function() {
        $("#myModal_btn").modal();
    });
    </script> -->
    <!-- //script for show signin and signup modal -->
    <!-- smooth dropdown -->
    <script>
    $(document).ready(function() {
        $('ul li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        });
    });
    </script>
    <!-- //smooth dropdown -->
    
    <!-- script for password match -->
    <!-- Banner Responsiveslides -->
    <script src="js/responsiveslides.min.js"></script>
    <script>
    // You can also use "$(window).load(function() {"
    $(function() {
        // Slideshow 4
        $("#slider3").responsiveSlides({
            auto: false,
            pager: true,
            nav: false,
            speed: 500,
            namespace: "callbacks",
            before: function() {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function() {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
    </script>
    <!-- // Banner Responsiveslides -->
    <!-- Product slider Owl-Carousel-JavaScript -->
    <script src="js/owl.carousel.js"></script>
    <script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 4,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            320: {
                items: 1,
            },
            568: {
                items: 2,
            },
            991: {
                items: 3,
            },
            1050: {
                items: 4
            }
        }
    });
    </script>
    <!-- //Product slider Owl-Carousel-JavaScript -->
    <!-- cart-js -->
    <script src="js/minicart.js">
    </script>
    <script>
    hub.render();

    hub.cart.on('new_checkout', function(evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
    });
    </script>
    <!-- //cart-js -->
    <!-- start-smooth-scrolling -->
    <script src="js/move-top.js"></script>
    <script src="js/easing.js"></script>
    <script>
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
    </script>
    <!-- //end-smooth-scrolling -->
    <!-- smooth-scrolling-of-move-up -->
    <script>
    $(document).ready(function() {
        /*
        var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear' 
        };
        */

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
    </script>
    <script src="js/SmoothScroll.min.js"></script>
    <!-- //smooth-scrolling-of-move-up -->
    <!-- Bootstrap core JavaScript
================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="js/bootstrap.js"></script>
</body>

</html>