<?php 
include 'db/dbConnection.php';
$id = $_GET['id'];
?>
<!DOCTYPE html>
<html lang="zxx">

<head>
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8" />
    <meta name="keywords" content="Fashion Hub Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
	SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola web design" />
    <script>
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- Custom Theme files -->
    <link href="css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
    <!-- shop css -->
    <link href="css/shop.css" type="text/css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <!-- Owl-Carousel-CSS -->
    <link href="css/style.css" type="text/css" rel="stylesheet" media="all">
    <!-- font-awesome icons -->
    <link href="css/fontawesome-all.min.css" rel="stylesheet">
    <!-- //Custom Theme files -->
    <!-- online-fonts -->
    <link href="//fonts.googleapis.com/css?family=Elsie+Swash+Caps:400,900" rel="stylesheet">
    <link href="//fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i"
          rel="stylesheet">
    <!-- //online-fonts -->
</head>
<style>
    .pt-100 {
        padding-top: 100px;
    }
    .pb-70 {
        padding-bottom: 70px;
    }
    .section-header {
        margin-bottom: 60px;
        text-align: center;
    }
    .section-header i {
        color: #ff007d;
        font-size: 50px;
        display: inline-block;
        margin-bottom: 10px;
    }
    .section-header h2 {
        font-weight: bold;
        font-size: 34px;
        margin: 0;
    }
    .section-header p {
        max-width: 500px;
        margin: 20px auto 0;
    }
    .single-publication {
        border: 1px solid #f2eee2;
        margin-bottom: 30px;
        position: relative;
        overflow: hidden;
    }
    .single-publication figure {
        position: relative;
        margin: 0;
        text-align: center;
    }
    .single-publication figure > a {
        background-color: #fafafa;
        display: block;
    }
    .single-publication figure ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
        position: absolute;
        right: -50px;
        top: 20px;
        transition: .6s;
        -webkit-transition: .6s;
    }
    .single-publication:hover figure ul {
        right: 15px;
    }
    .single-publication figure ul li a {
        display: inline-block;
        width: 35px;
        height: 35px;
        text-align: center;
        font-size: 15px;
        background: #ff007d;
        margin-bottom: 7px;
        border-radius: 50%;
        line-height: 35px;
        color: #fff;
    }
    .single-publication figure ul li a:hover {
        color: #fff;
        background: #e50663;
    }
    .single-publication .publication-content {
        text-align: center;
        padding: 20px;
    }
    .single-publication .publication-content .category {
        display: inline-block;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        color: #ff007d;
        font-weight: 600;
    }
    .single-publication .publication-content h3 {
        font-weight: 600;
        margin: 8px 0 10px;
        font-size: 20px;
    }
    .single-publication .publication-content h3 a {
        color: #1f2d30;
    }
    .single-publication .publication-content h3 a:hover {
        color: #ff007d;
    }
    .single-publication .publication-content ul {
        list-style-type: none;
        padding: 0;
        margin: 0;
        margin-bottom: 15px;
    }
    .single-publication .publication-content ul li {
        display: inline-block;
        font-size: 18px;
        color: #fec42d;
    }
    .single-publication .publication-content .price {
        font-size: 18px;
        color: #ff007d;
    }
    .single-publication .publication-content .price span {
        color: #6f6f6f;
        text-decoration: line-through;
        padding-left: 5px;
        font-weight: 300;
    }
    .single-publication .add-to-cart {
        position: absolute;
        right: 0;
        bottom: 0;
        left: 0;
        background: #fff;
        opacity: 0;
        visibility: hidden;
        text-align: center;
        -webkit-transform: scale(.7);
        transform: scale(.7);
        height: 105px;
        -moz-transition: .4s;
        -webkit-transition: .4s;
        transition: .4s;
    }
    .single-publication:hover .add-to-cart {
        visibility: visible;
        transform: scale(1);
        -webkit-transform: scale(1);
        opacity: 1;
    }
    .single-publication .add-to-cart .default-btn {
        margin-top: 28px;
        padding: 8px 25px;
        font-size: 14px;
    }
    .single-publication .category {
        margin: 0;
    }
    .single-publication .add-to-cart .default-btn {
        margin-top: 28px;
        padding: 8px 25px;
        font-size: 14px;
    }
    .default-btn {
        background-color: #ff007d;
        color: #fff;
        border: 1px solid #ff007d;
        display: inline-block;
        padding: 10px 30px;
        border-radius: 30px;
        text-transform: uppercase;
        font-weight: 600;
        font-family: 'Open Sans', sans-serif;
    }
    a:hover {
        color: #fff;
        text-decoration: none;
    }
</style>
<body>
<!-- header -->
<?php include 'header.php'; ?>
<!-- //header -->
<!-- banner -->

<!-- //banner -->

<!-- product tabs -->
<section class="tabs_pro py-md-5 pt-sm-3 pb-5">
    <h5 class="head_agileinfo text-center text-capitalize pb-5">
        <span>N</span>ew Finds</h5>
    <div class="tabs tabs-style-line pt-md-5">
        <section class="our-publication pt-100 pb-70">
            <div class="container">
                <div class="row">

                    <?php
                    $sql ="SELECT * FROM `product_tbl` WHERE pro_sub_id=$id";
                    $result = mysqli_query($connection,$sql);
                    while($dataRow=mysqli_fetch_assoc($result)){
                        $sql1 ="SELECT * FROM `image_table` WHERE image_table.pro_id = $dataRow[pro_id] limit 1";
                        $result1 = mysqli_query($connection,$sql1);
                        while($dataRow1=mysqli_fetch_assoc($result1)){
                            $image = $dataRow1['image_name'];
                        }
                    ?>

                    <div class="col-sm-6 col-lg-3">
                        <div class="single-publication">
                            <figure>
                                <a href="buy?id=<?php echo $dataRow['pro_id']?>">
                                    <img src="admin/galleryImg/<?php echo $image?>" alt="Publication Image" style="height: 200px">
                                </a>

                                <!-- <ul>
                                    <li><a href="#" title="Add to Favorite"><i class="fa fa-heart"></i></a></li>
                                    <li><a href="#" title="Add to Compare"><i class="fa fa-refresh"></i></a></li>
                                    <li><a href="#" title="Quick View"><i class="fa fa-search"></i></a></li>
                                </ul> -->
                            </figure>

                            <div class="publication-content">
                                <span class="category">Book</span>
                                <h3><a href="#">Think Python</a></h3>
                                <ul>
                                    <li><i class="icofont-star"></i></li>
                                    <li><i class="icofont-star"></i></li>
                                    <li><i class="icofont-star"></i></li>
                                    <li><i class="icofont-star"></i></li>
                                    <li><i class="icofont-star"></i></li>
                                </ul>
                                <h4 class="price">$119 <span>$299</span></h4>
                            </div>

                            <div class="add-to-cart">
                                <a href="buy?id=<?php echo $dataRow['pro_id']?>" class="default-btn">Add to Cart</a>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </section>
    </div>
</section>
<!-- //product tabs -->
<!-- insta posts -->

<!-- //insta posts -->
<!-- footer -->
<?php include 'footer.php'; ?>
<!-- //footer -->

<!-- js -->
<script src="js/jquery-2.2.3.min.js"></script>

<!-- //script for show signin and signup modal -->
<!-- smooth dropdown -->
<script>
    $(document).ready(function() {
        $('ul li.dropdown').hover(function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(200);
        }, function() {
            $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(200);
        });
    });
</script>
<!-- //smooth dropdown -->

<!-- script for password match -->
<!-- Banner Responsiveslides -->
<script src="js/responsiveslides.min.js"></script>
<script>
    // You can also use "$(window).load(function() {"
    $(function() {
        // Slideshow 4
        $("#slider3").responsiveSlides({
            auto: false,
            pager: true,
            nav: false,
            speed: 500,
            namespace: "callbacks",
            before: function() {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function() {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>
<!-- // Banner Responsiveslides -->
<!-- Product slider Owl-Carousel-JavaScript -->
<script src="js/owl.carousel.js"></script>
<script>
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        items: 4,
        loop: false,
        margin: 10,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplayHoverPause: false,
        responsive: {
            320: {
                items: 1,
            },
            568: {
                items: 2,
            },
            991: {
                items: 3,
            },
            1050: {
                items: 4
            }
        }
    });
</script>
<!-- //Product slider Owl-Carousel-JavaScript -->
<!-- cart-js -->
<script src="js/minicart.js">
</script>
<script>
    hub.render();

    hub.cart.on('new_checkout', function(evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {}
        }
    });
</script>
<!-- //cart-js -->
<!-- start-smooth-scrolling -->
<script src="js/move-top.js"></script>
<script src="js/easing.js"></script>
<script>
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event) {
            event.preventDefault();

            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script>
    $(document).ready(function() {
        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<script src="js/SmoothScroll.min.js"></script>
<!-- //smooth-scrolling-of-move-up -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="js/bootstrap.js"></script>
</body>

</html>