<?php
include '../db/dbConnection.php';
include 'header.php';

$category = $_POST['category_id'];
$image = $_FILES['image']['name'];
?>

<main class="app-content">

<?php
    // File upload configuration
    $targetDir = 'galleryImg/';
    $allowTypes = array('jpg', 'png', 'jpeg', 'gif');

    $statusMsg = $errorMsg = $insertValuesSQL = $errorUpload = $errorUploadType = '';
    if (!empty(array_filter($_FILES['image']['name']))) {
        foreach ($_FILES['image']['name'] as $key => $val) {
            // File upload path
            $fileName = basename($_FILES['image']['name'][$key]);
            $targetFilePath = $targetDir.$fileName;

            // Check whether file type is valid
            $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);
            if (in_array($fileType, $allowTypes)) {
                // Upload file to server
                if (move_uploaded_file($_FILES['image']['tmp_name'][$key], $targetFilePath)) {
                    // Image db insert sql
                    $insertValuesSQL .= "('','$category',now(),'$fileName'),";
                } else {
                    $errorUpload .= $_FILES['image']['name'][$key].', ';
                }
            } else {
                $errorUploadType .= $_FILES['image']['name'][$key].', ';
            }
        }

        if (!empty($insertValuesSQL)) {
            $insertValuesSQL = trim($insertValuesSQL, ',');
            // Insert image file name into database
            $insert = $connection->query("INSERT INTO image_tbl (`img_id`,`category_img_id`,`img_added_date`,`image`) VALUES $insertValuesSQL");
            if ($insert) {
                $statusMsg = 'Well done! Image Uploaded Success.';
            } else {
                $statusMsg = 'Sorry, there was an error uploading your file.';
            }
        }
    } else {
        $statusMsg = 'Please select a file to upload.';
    }
 ?>
    <form action="list-image">
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="alert alert-dismissible alert-success">
                    <button class="close" type="button" data-dismiss="alert">×</button><strong><?php echo $statusMsg; ?></strong>
                </div>
            </div>
        </div>
        <div align="center">
            <button type="submit" class="btn btn-success" type="button">Ok</button>

        </div>
    </form>
</main>
<?php
 include 'footer.php';
 ?>